import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MatrDemo {
    public static void returnValue (float m) {
        System.out.println(m);
    }

    public static void putArr(int[] X) //����������� ����� ������ ������� � ��������
    {
        for (int i = 0; i < X.length; i++)
            System.out.printf("%d ", X[i]);
        System.out.println();
    }

    public static void putMatr(float[][] Y) {//����� ������� � ���� ���������
        for (int i = 0; i < Y.length; i++) {
            for (int j = 0; j < Y[0].length; j++)
                System.out.printf("% 7.2f", Y[i][j]);
            System.out.println();
        }
    }

    public static void main(String[] args) throws IOException {
        String fileName;//��� �����
        String line = "";//�������� ������
        String flag = "";//��� ������ �� ����� ����� � ���������
        float[][] matr;//������ �� �������-��������
        //��������� ����� ��� ������������� �����
        BufferedReader br = new BufferedReader(
                new InputStreamReader(System.in));
        do{
            System.out.println("Enter the name of the matrix");
            fileName = (br.readLine()).trim();//����� ����� ����� ����� �
            //���� ��������� ������ ��� � �������� ������� � ������ � � ���-��
            matr = IO.inpMatr(fileName);//���� ������� �� �����
            if (matr != null){
                System.out.println("Matrix entered successfully");
                //������� ������ ������ ���������������� ������� �� ���� ���-������ �������
                IntellectMatr imatr = new IntellectMatr(matr);
                //������� �������� ������� � ���� ���������
                System.out.println("Matrix introduced:");
                imatr.putMatr();
                //������������ ������� �������� ������
                //���������������� ������� � ������� �������������
                //���������� ��������� � ���� ���������
                System.out.println("Number of negative elements in matrix rows:");
                putArr(imatr.negativeCntVector());
                System.out.println("Sum of the elements of the main diagonal:");
                System.out.println(imatr.sumDiagonal());
                IntellectMatr imatr2 = new IntellectMatr(matr);
                System.out.println("Determinant of matrix is:");
                IntellectMatr imatr3 = new IntellectMatr(matr);
                returnValue(imatr3.getValue());
                System.out.println("Transposed matrix :");
                IntellectMatr imatr4 = new IntellectMatr(matr);
                putMatr(imatr4.TranspMatr());
            }
            System.out.println("Continue?: Yes - <Enter>, No - <n>");
            flag = br.readLine().trim();
        } while (!flag.equals("n"));//���� � ��������� �� ������ ������ n
    }
    //���� � ��������� ����������������, �.�. ���������� ��� ������� Enter
}
