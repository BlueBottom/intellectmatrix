import java.io.*;

public class IO {
    //������ ������ - ����������� (�� ����� ������������
    //��� �������� �������� ����� ������)
    public static float[][] inpMatr(String fileName) throws IOException{
        //���� ������� �� ���������� ����� � ������ fileName
        float[][] matr;//�������-��������
        String line;//������, ��������� �� �����
        int i = 0;//����� ������ �������
        String[] numbers;//������ �� ������ ����, ��������� � ������
        int n;//����� ����� �������
        int m;//����� �������� �������
        BufferedReader inp = null;//������ �� ������� �����
        try{//1 - ������������ ���������� �����-������
            //��������� ����� �����
            //����� ���������� ���������� FileNotFoundException, IOException
            inp = new BufferedReader(new FileReader(fileName));
            //����� - ��������� ������ - ������ ������ �� �����
            //�������� ����� ���������� ���������� IOException
            //���������� � ������ ����� ������ ������ ��� ������ � ������ ���������
            while ((line = inp.readLine()) != null) if (!line.trim().equals("")) break;
            //������� ����� ������ ����� ����� � ����� ��������
            try{//2 - ������������ ������������ ����� ������������
                //� ����������, ����������� ��� �������������� �� String � ���-��
                if (line == null)//���� ������ ����� �����
                    //���������� ��������� MyException
                    throw new MyException(String.format("File %s is empty ", fileName));
                numbers = line.trim().split("\\s+");//������ ������ �� ��������� �����
                if (numbers.length < 2)//�� ������ ����� �������� �������
                    //���������� ���������� MyException
                    throw new MyException(String.format(
                            "The number of matrix  columns isn't specified in the file %s ", fileName));
                //����� ���������� ���������� NumberFormatException
                n = Integer.parseInt(numbers[0]); m = Integer.parseInt(numbers[1]);
                if ((n <= 0) || (m <= 0))//����������� ������ ����� ������ ��� ��������
                    //���������� ���������� MyException
                    throw new MyException(String.format(
                            "The number of rows or columns in the file %s is not set cor-rectly",
                            fileName));
                matr = new float[n][m];//������� �������-��������
                //��������� ������ ������� �� �����
                //��� ����� ����� ���������� ���������� IOException
                while ((line = inp.readLine()) != null){//���� ��������� ������
                    //���� ������ ������ ��� ������� �� ����� ��������
                    //���������� �
                    line = line.trim();
                    if (line.equals("")) continue;
                    //������ ������ �� ��������� �����(�����)
                    numbers = line.split("\\s+");
                    if (numbers.length < m)//� ������ ������ m �����,
                        //� ������ ���� m(��� ������) ����� (������ ����������-��)
                        //���������� ���������� IOException
                        throw new MyException(String.format(
                                "Not enough numbers in the row %d from the file %s ", i, fileName));
                    //����� ����� ���������� ���������� NumberFormatException
                    for (int j = 0; j < m; j++) matr[i][j] = Float.parseFloat(numbers[j]);
                    i++;//����������� ����� ��������� ������
                    if (i == n) break;//������� ��������� � ����� ����� �����
                    //���� � ����� ��� ���� ������ - ��� ������������
                }//while
                if (i < n)//������� ������ �����, ��� �������� � ������ �������� ������ �����
                    //���������� ���������� MyException
                    throw new MyException(String.format(
                            "Missing lines in file %s ", fileName));
            }//try 2
            //����� ������� ���������� ������������ catch ���� �� ������
            catch (MyException e){
                //���������� ���������� ��� try 2 (������������ ��������� �����)
                System.out.println(e); return null;}
            catch (NumberFormatException e){
                //���������� ���������� ��� try 2 (������ �������������� ���-���)
                System.out.printf("In line %d of matrix from file %s", i, fileName);
                System.out.println(
                        " Invalid character sequence found");
                return null;}
        }//try 1
        //������� ���������� ������ catch ��� try 1 ������������ ���, ���
        //����� FileNotFoundException �������� ���������� IOException
        catch (FileNotFoundException e){
            //���� �� ������ - ���������� ��� try 1
            System.out.printf("File %s not found\n", fileName);
            return null;}
        catch (IOException e){
            //������ ����� ������ - ���������� ��� try 1
            System.out.printf("Error when entering data from a file %s\n",
                    fileName);
            return null;}
        finally{ if (inp != null) inp.close();//��������� �����
            //��� �������� ����� ���������� ���������� IOException,
            //������� � ���� ����� ��������� �� ��������������� � ��
            //��������������, ������� � ��������� ������ inpMatr
            //������ �������������� ����������� throws IOException
        }//finally
        return matr;
    }
}//class IO
